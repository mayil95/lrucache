package org.bayramov.lrucache;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class LRUCacheTest {

    private  LRUCache lruCache;

    @BeforeEach
     void beforeEach(){
        //reset cache before each test case
        lruCache = new LRUCache(4);
    }

    @AfterAll
    static void tearDown(){
        // ...
    }

    @Test
    void whenNegativeCapacity_ThrowsException(){
       assertThrows(IllegalArgumentException.class, ()->new LRUCache(-1));
    }

    @Test
    void whenLookupByKey_ExpectValidValue(){
        lruCache.put(618, new Emoji("U+1F618"));
        var value = lruCache.get(618);
        assertNotNull(value);
        assertEquals("U+1F618" ,value.getUnicode());
    }

    @Test
    void whenPuttingKeyValuePairMoreThanCapacity_ExpectValidCapacity(){
        lruCache.put(618, new Emoji("U+1F618"));
        lruCache.put(648, new Emoji("U+1F648"));
        lruCache.put(440, new Emoji("U+1F440"));
        lruCache.put(648, new Emoji("U+1F648"));
        lruCache.put(160, new Emoji("U+1F60E"));
        lruCache.put(144, new Emoji("U+1F44F"));
        assertEquals(4 , lruCache.getSize());
    }

}


