package org.bayramov.lrucache;

class Emoji {
    private String unicode;
    private String name;
    private String category;

    public Emoji(String unicode){
        this.unicode = unicode;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
