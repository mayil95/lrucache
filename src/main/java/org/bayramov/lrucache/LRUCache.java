package org.bayramov.lrucache;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {

    public static class Node {
        private  Emoji value;
        private final int key;
        private Node next;
        private Node prev;

        public Node(int key, Emoji value){
            this.key = key;
            this.value = value;
        }

    }

    private final Node head;
    private final Node tail;
    private final Map<Integer, Node> map;
    private final int capacity;



    public LRUCache(int capacity) {
        if (capacity < 1){
            throw new IllegalArgumentException("cache capacity can't be less than 1");
        }
        this.capacity = capacity;
        //initialize others with defaults
        this.map = new HashMap<>();
        //in order not deal with edge cases, it is recommended that
        //initialize your head and tail nodes with null values
        this.head = new Node(-1, null);
        this.tail = new Node(-1, null);
        head.next = tail;
        tail.prev = head;
    }

    public Emoji get(int key) {
        Node node = map.get(key);
        if(node == null) return null;
        //remove key
        removeNode(node);
        //add that node to head of list
        addNode(node, head);
        return node.value;
    }

    public void put(int key, Emoji value) {
        Node node = map.get(key);
        if (node != null){
            //update value of existing key
            update(node, value);
        }else {
            //add key value
            add(key, value);
        }
    }

private void add(int key, Emoji value){
    Node curr = new Node(key, value);
    //add node to the head of list
    addNode(curr, head);
    //put key-value pair to map
    map.put(key, curr);
    //check map size exceeds predefined capacity
    if(map.size() > capacity){
        //evict the least recently used key
        evict();
    }
}

    private void update(Node node, Emoji newValue){
        //update value in the node
        node.value = newValue;
        //remove node
        removeNode(node);
        //add node to the head
        addNode(node, head);
        //update value in the map
        map.put(node.key, node);
    }

 private void evict(){
     //remove tail node(as we keep always tail and head nodes stable,
     //therefore we will remove prev node of tailNode)
     remove(tail.prev);
 }

 private void remove(Node node){
     //remove current node value from map
     map.remove(node.key);
     //remove current node from linked list
     removeNode(node);
 }

 private void removeNode(Node node){
     //remove current node from linked list
     Node prevNode = node.prev;
     Node nextNode = node.next;
     prevNode.next = nextNode;
     nextNode.prev = prevNode;
 }

    private void addNode(Node curr, Node before){
        Node after = before.next;
        before.next = curr;
        curr.next = after;
        after.prev = curr;
        curr.prev = before;
    }

    public int getSize(){
      return map.size();
    }
}

